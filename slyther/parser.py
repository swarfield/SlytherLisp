# To Whomever reads this code, I'm sorry for the monstrosity I've made...
"""
This module defines parsing utilities in SlytherLisp.

>>> parser = parse(tokenize('(print "Hello, World!")'))
>>> se = next(parser)
>>> se
(print "Hello, World!")
>>> type(se)
<class 'slyther.types.SExpression'>

"""

import re
from slyther.types import SExpression, Symbol, String, Quoted, NIL, cons


__all__ = ['tokenize', 'parse']


def tokenize(code):
    r"""
    This is a *generator function* that splits a piece of code into
    lexical tokens, emitting whole tokens each time one is encountered.

    >>> list(tokenize(" (define (do-123 fun) \n (map fun '(1 2 3))) \n"))
    ...                                  # doctest: +NORMALIZE_WHITESPACE
    ['(', 'define', '(', 'do-123', 'fun', ')',
        '(', 'map', 'fun', "'(", '1', '2', '3', ')', ')', ')']

    String literals should be emitted as a whole token, unparsed:

    >>> list(tokenize(r'(print "hello \"world\"!")'))
    ['(', 'print', '"hello \\"world\\"!"', ')']
    >>> list(tokenize('(print "hello world\n!")'))
    ['(', 'print', '"hello world\n!"', ')']
    >>> list(tokenize(r'(print "hello world\n!")'))
    ['(', 'print', '"hello world\\n!"', ')']

    Since symbols cannot start with a digit, this function should separate the
    numerical literal from the symbol when things like this happen:

    >>> list(tokenize('(print 8-dogcows)'))
    ['(', 'print', '8', '-dogcows', ')']
    >>> list(tokenize('(print -8.0-dogcows)'))
    ['(', 'print', '-8.0', '-dogcows', ')']

    And since symbols can have digits and dots in the middle, make sure these
    are parsed properly:

    >>> list(tokenize('(print dogcows-8.0)'))
    ['(', 'print', 'dogcows-8.0', ')']

    Quoted things should be emitted as quoted:

    >>> list(tokenize("(print 'hello-world '4 '8.0)"))
    ['(', 'print', "'hello-world", "'4", "'8.0", ')']

    And since symbols and quoted things cannot contain quotes in the middle:

    >>> list(tokenize("(print'hello-world'4'8.0)"))
    ['(', 'print', "'hello-world", "'4", "'8.0", ')']
    >>> list(tokenize("(print'8.0-dogcows)"))
    ['(', 'print', "'8.0", '-dogcows', ')']
    >>> list(tokenize("(print'-8.0-dogcows)"))
    ['(', 'print', "'-8.0", '-dogcows', ')']

    Shebang lines **only at the front of the string, before any whitespace**
    should be ignored, and not emitted as a token:

    >>> list(tokenize("#!/usr/bin/env slyther\n(print 1)\n"))
    ['(', 'print', '1', ')']
    >>> list(tokenize("#!slyther\n(print 1)"))
    ['(', 'print', '1', ')']
    >>> list(tokenize(" #!/usr/bin/env slyther\n(print 1)\n"))
    ['#!/usr/bin/env', 'slyther', '(', 'print', '1', ')']

    Comments start at a semicolon and go until the end of line (or,
    potentially the end of the input string). Note that string literals
    might contain semicolons: these don't start a comment, beware.
    Comments should not be emitted from the tokenizer.

    >>> list(tokenize(
    ...     '(here-comes; a comment!\n "no comment ; here";comment()\n)'))
    ['(', 'here-comes', '"no comment ; here"', ')']
    >>> list(tokenize('; commments can contain ; inside them\n'))
    []

    When an error is encountered, ``SyntaxError`` should be raised:

    >>> list(tokenize("(' )"))        # what are you trying to quote?
    Traceback (most recent call last):
        ...
    SyntaxError: malformed tokens in input
    >>> list(tokenize("'"))
    Traceback (most recent call last):
        ...
    SyntaxError: malformed tokens in input
    >>> list(tokenize("(''x)"))
    Traceback (most recent call last):
        ...
    SyntaxError: malformed tokens in input
    >>> list(tokenize(r'(print "Hello, World!\")'))   # unclosed string
    Traceback (most recent call last):
        ...
    SyntaxError: malformed tokens in input

    Don't worry about handling unmatched parens, etc. This will be the
    parser's job! In other words, the tokenizer should be no smarter
    than it needs to be to do its job.

    >>> list(tokenize("((("))
    ['(', '(', '(']

    >>> list(tokenize("<"))
    ['<']
    >>> list(tokenize("-"))
    ['-']
    >>> list(tokenize("- "))
    ['-']
    >>> list(tokenize(" -"))
    ['-']
    >>> list(tokenize("#!/usr/bin/env slyther\n"))

    .. hint::

        Make good use of regular expression(s), and your life will be
        much easier. I was able to implement mine in only a few lines
        of code, using one giant regular expression!

    """
    # TODO: ADD DOCSTRINGS TO TEST MATH, AND PROPER STRINGS
    semi_in_string = re.compile(r'".*;.*"')
    incorrect_quotation = re.compile(r'(\'\s|\'$|\'\')')
    find_correct_string = re.compile(r'[^\\]"')

    # Handle User Comments //If symbol starts with ; remove if
    if (semi_in_string.search(code) is not None):
        # Protects ';'s in strings by using the greee semicolon
        code = re.sub(r'".*;.*"',                      # Find this Regex
                      re.split(r'(".*;.*")', code)[1]  # Find the string
                      .replace(';', ';'),              # Replace with Greek
                      code)                            # Replace the greek

        # Removes comments
        code = re.sub(r';.*\n?', '', code)

        # Reverts back to normal semis in strings
        code = re.sub(r'".*;.*"',                      # Find this regex
                      re.split(r'(".*;.*")', code)[1]  # Find the string
                      .replace(';', ';'),              # Replace the greek semi
                      code)                            # From the code string

    else:
        code = re.sub(r';.*\n?', '', code)  # Still remove all comments

    # Check for correct quotation
    if (incorrect_quotation.search(code) is not None):
        raise SyntaxError("malformed tokens in input")

    # Check for correct strings
    i = 0
    for _ in re.finditer(find_correct_string, code):
        i += 1
    if (i % 2 == 1):
        raise SyntaxError("malformed tokens in input")

    for a_token in re.finditer(tokenize.tokens, code):
        yield a_token[1]
    # raise NotImplementedError("Deliverable 2")


tokenize.tokens = re.compile(r'''             # TODO: Write new doc strings
    (?:^\#\!.+\n)? # Match Shebang
    ('?\(|\)
    |'?"(\\"|[^"])*"
    |'?-?\d+(\.\d?)?
    |'?\S[^\s\(\)\']*
    )
    ''', re.VERBOSE)


def parse(tokens):
    r"""
    This generator function takes a token generator object (from the
    ``tokenize`` function) and generates AST elements.

    >>> tokens = [
    ...     '(', 'define', '(', 'do-123', 'fun', ')',
    ...         '(', 'map', 'fun', "'(", '1', '2', '3', ')', ')', ')']
    >>> parser = parse(iter(tokens))
    >>> se = next(parser)
    >>> se
    (define (do-123 fun) (map fun '(1 2 3)))
    >>> type(se)
    <class 'slyther.types.SExpression'>
    >>> type(se.car)
    <class 'slyther.types.Symbol'>
    >>> quoted_list = se.cdr.cdr.car.cdr.cdr.car
    >>> quoted_list
    '(1 2 3)
    >>> type(quoted_list)
    <class 'slyther.types.Quoted'>
    >>> type(quoted_list.elem)
    <class 'slyther.types.SExpression'>

    Another example, showing how numerics work:

    >>> tokens = ['(', 'print', '1', "'-2", "'3.0", '-4.0', ')']
    >>> parser = parse(iter(tokens))
    >>> se = next(parser)
    >>> numbers = se.cdr
    >>> numbers
    (1 '-2 '3.0 -4.0)
    >>> [type(x) for x in numbers]   # doctest: +NORMALIZE_WHITESPACE
    [<class 'int'>,
     <class 'slyther.types.Quoted'>,
     <class 'slyther.types.Quoted'>,
     <class 'float'>]

    The parser should be able to handle literals that even occur
    outside of an s-expression:

    >>> tokens = ['1', '(', '2', '3', ')', '"string lit"']
    >>> parser = parse(iter(tokens))
    >>> next(parser)
    1
    >>> next(parser)
    (2 3)
    >>> next(parser)
    "string lit"

    The parser should make use of ``parse_strlit`` to parse string
    literals:

    >>> tokens = [r'"this is my\" fancy\n\estring literal"']
    >>> parser = parse(iter(tokens))
    >>> next(parser)
    "this is my\" fancy\n\x1bstring literal"

    When an s-expression is not closed, a ``SyntaxError`` should be
    raised:

    >>> tokens = ['(', ')', '(', 'print']
    >>> parser = parse(iter(tokens))
    >>> next(parser)
    NIL
    >>> next(parser)
    Traceback (most recent call last):
        ...
    SyntaxError: s-expression not closed

    Likewise, when there's too many closing parens:

    >>> tokens = ['(', ')', ')', '(', 'print', ')']
    >>> parser = parse(iter(tokens))
    >>> next(parser)
    NIL
    >>> next(parser)
    Traceback (most recent call last):
        ...
    SyntaxError: too many closing parens

    #TODO: Email jack this doc string
    >>> list(tokenize("'(x y z)"))
    ["'(", 'x', 'y', 'z', ')']

    >>> list(tokenize("(x y z)"))
    ['(', 'x', 'y', 'z', ')']

    >>> list(tokenize("'(x y z (a b c))"))
    ["'(", 'x', 'y', 'z', '(', 'a', 'b', 'c', ')', ')']

    >>> next(parse(tokenize("(x y z (a b c))")))
    (x y z (a b c))

    >>> next(parse(tokenize("'(x y z (a b c))")))
    '(x y z (a b c))

    >>> next(parse(tokenize("'((a b (c)) (1 (2) 3))")))
    '((a b (c)) (1 (2) 3))
    >>> p = parse(tokenize("'((a b (c)) (1 (2) 3))"))
    >>> for x in p:
    ...     print(x)
    '((a b (c)) (1 (2) 3))

    """

    the_stack = []

    while True:
        # If the current parse is not an opening bracket return it
        if len(the_stack) > 0:
            if the_stack[0] != Symbol('(') and the_stack[0] != Symbol("'("):
                yield the_stack[0]
                the_stack.clear()

        try:
            a_token = next(tokens)
        except StopIteration:
            if the_stack:
                raise SyntaxError("s-expression not closed")
            raise StopIteration

        if a_token == ')':  # End of an SE
            a_bool = True

            try:
                if (the_stack[-1] == Symbol('(')):
                    a_bool = False
                    the_stack.pop()
                    cdr = NIL
            except IndexError:
                raise SyntaxError("too many closing parens")

            cdr = NIL

            while a_bool:
                if the_stack == []:
                    raise SyntaxError("Unclosed SE")

                car = the_stack.pop()

                if car == "'(":
                    cdr = Quoted(cdr)
                    break
                if car == Symbol('('):
                    break

                cdr = SExpression(car, cdr)

            the_stack.append(cdr)

        elif a_token[0] == "'" and a_token != "'(":  # quoted
            a_list = [a_token[1:]]
            the_quote = Quoted(next(parse(iter(a_list))))
            the_stack.append(the_quote)

        elif bool(parse.is_integer.fullmatch(a_token)):  # Integer
            the_stack.append(int(a_token))

        elif bool(parse.is_float.match(a_token)):  # Float
            the_stack.append(float(a_token))

        elif a_token[0] == '"':  # String
            the_stack.append(parse_strlit(a_token))

        else:  # It's a token
            the_stack.append(Symbol(a_token))


parse.counter = 0  # Counts the the parens in the parser
parse.is_integer = re.compile(r'-?[0-9]+')
parse.is_float = re.compile(r'-?\d+\.\d+')


def parse_strlit(tok):
    r"""
    This function is a helper method for ``parse``. It takes a string
    literal, raw output from the tokenizer, and converts it to a
    ``slyther.types.String``.

    It should support the following translations:

    +-----------------+--------------------+
    | Escape Sequence | Resulting Output   |
    +=================+====================+
    | ``\0``          | ASCII Value 0      |
    +-----------------+--------------------+
    | ``\a``          | ASCII Value 7      |
    +-----------------+--------------------+
    | ``\b``          | ASCII Value 8      |
    +-----------------+--------------------+
    | ``\e``          | ASCII Value 27     |
    +-----------------+--------------------+
    | ``\f``          | ASCII Value 12     |
    +-----------------+--------------------+
    | ``\n``          | ASCII Value 10     |
    +-----------------+--------------------+
    | ``\r``          | ASCII Value 13     |
    +-----------------+--------------------+
    | ``\t``          | ASCII Value 9      |
    +-----------------+--------------------+
    | ``\v``          | ASCII Value 11     |
    +-----------------+--------------------+
    | ``\"``          | ASCII Value 34     |
    +-----------------+--------------------+
    | ``\\``          | ASCII Value 92     |
    +-----------------+--------------------+
    | ``\x##``        | Hex value ``##``   |
    +-----------------+--------------------+
    | ``\0##``        | Octal value ``##`` |
    +-----------------+--------------------+

    >>> parse_strlit(r'"\0"')
    "\x00"
    >>> parse_strlit(r'"\e"')
    "\x1b"
    >>> parse_strlit(r'"\x41"')
    "A"
    >>> parse_strlit(r'"\x53\x6c\x79\x74\x68\x65\x72\x4C\x69\x73\x70"')
    "SlytherLisp"
    >>> parse_strlit(r'"this is my\" fancy\n\estring literal"')
    "this is my\" fancy\n\x1bstring literal"

    Patterns which do not match the translations should be left alone:

    >>> parse_strlit(r'"\c\d\xzz"')
    "\\c\\d\\xzz"

    Octal values should only expand when octal digits (0-7) are used:

    >>> parse_strlit(r'"\077"')
    "?"
    >>> parse_strlit(r'"\088"') # a \0, followed by two 8's
    "\x0088"

    Even though this is similar to Python's string literal format,
    you should not use any of Python's string literal processing
    utilities for this: tl;dr do it yourself.

    .. hint::

        Implement similar to how you tokenized and parsed the source
        code: break the string into tokens (either escape sequences
        or letters), then translate and join the tokens.

    Various other test cases follow:

    >>> import string
    >>> parse_strlit('"'
    ...     + ''.join('\\' + c for c in string.ascii_lowercase)
    ...     + '"')
    "\x07\x08\\c\\d\x1b\x0c\\g\\h\\i\\j\\k\\l\\m\n\\o\\p\\q\r\\s\t\\u\x0b\\w\\x\\y\\z"
    >>> parse_strlit('"'
    ...     + ''.join('\\' + c for c in string.ascii_uppercase)
    ...     + '"')
    "\\A\\B\\C\\D\\E\\F\\G\\H\\I\\J\\K\\L\\M\\N\\O\\P\\Q\\R\\S\\T\\U\\V\\W\\X\\Y\\Z"

    """
    dict_of_ASCII = {'\\0': 0, '\\a': 7, '\\b': 8, '\\e': 27, '\\f': 12,
                     '\\n': 10, '\\r': 13, '\\t': 9, '\\v': 11, '\\"': 34,
                     '\\\\': 92}

    # Match Escaped charecters
    escaped = re.compile(r'\\[0abefntvr"\\]', re.DOTALL)
    octal_num = re.compile(r'\\0[0-7]+')
    hex_num = re.compile(r'\\x[\da-fA-F]+')

    # Replace escaped hexadecimal
    tok = hex_num.sub(lambda m: chr(int(str('0' + m[0][1:]), 16)), tok)

    # Replace escaped octal
    tok = octal_num.sub(lambda m: chr(int(m[0][2:], 8)), tok)

    # Replace escaped charecters
    tok = escaped.sub(lambda m: chr(dict_of_ASCII[m[0]]), tok)

    # Remove quotation marks
    tok = tok[1:-1]
    return String(tok)
